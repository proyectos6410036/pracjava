//obtener el objeto button de calcular 
// Obtener elementos del DOM
const btnCalcular = document.getElementById("btnCalcular");
const valorAutoInput = document.getElementById("valorAuto");
const porcentajeInput = document.getElementById("porcentaje");
const plazosSelect = document.getElementById("plazos");
const pagoinicialInput = document.getElementById("pagoinicial");
const totalfinInput = document.getElementById("totalfin");
const pagoMensualInput = document.getElementById("pagoMensual");
const registrosLista = document.getElementById("registros"); // Cambiar a "registrosLista"

// Agregar un evento click al botón de calcular
btnCalcular.addEventListener("click", function () {
    // Obtener los valores de los inputs
    const valorAuto = parseFloat(valorAutoInput.value);
    const porcentaje = parseFloat(porcentajeInput.value);
    const plazos = parseFloat(plazosSelect.value);

    // Realizar cálculos
    const pagoInicial = (valorAuto * porcentaje) / 100;
    const totalFinanciar = valorAuto - pagoInicial;
    const pagoMensual = totalFinanciar / plazos;

    // Mostrar los resultados en los campos de texto
    pagoinicialInput.value = pagoInicial.toFixed(2);
    totalfinInput.value = totalFinanciar.toFixed(2);
    pagoMensualInput.value = pagoMensual.toFixed(2);

    // Crear un nuevo elemento de lista para el registro
    const registroItem = document.createElement("li");
    registroItem.innerHTML = `
        Pago Inicial: $${pagoInicial.toFixed(2)}<br>
        Total a Financiar: $${totalFinanciar.toFixed(2)}<br>
        Pago Mensual: $${pagoMensual.toFixed(2)} por ${plazos} meses
    `;

    // Agregar el registro a la lista en "Registros"
    registrosLista.appendChild(registroItem);
});

function limpiar() {
    // Limpiar los campos de entrada
    document.getElementById("valorAuto").value = "";
    document.getElementById("porcentaje").value = "";
    document.getElementById("plazos").value = "";
    document.getElementById("pagoinicial").value = "";
    document.getElementById("totalfin").value = "";
    document.getElementById("pagoMensual").value = "";
}
