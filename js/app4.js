// Selecciona el elemento ul donde mostrar la lista de alumnos
const ulElement = document.getElementById("alumnos-ul");

// Array con 10 nuevos alumnos
const nuevosAlumnos = [
    {
        "matricula": "2021030008",
        "nombre": "Landeros Andrade Maria Estrella",
        "grupo": "TI-73",
        "carrera": "Tecnologías de la Información",
        "foto": "/img/2021030008.jpg"
    },
    {
        "matricula": "2021030266",
        "nombre": "Gonzalez Ramirez Jose Manuel",
        "grupo": "TI-73",
        "carrera": "Tecnologías de la Información",
        "foto": "/img/2021030266.jpg"
    },
    {
        "matricula": "2020030714",
        "nombre": "Ruiz Guerrero Axel Jovani",
        "grupo": "TI-73",
        "carrera": "Tecnologías de la Información",
        "foto": "/img/2020030714.jpg"
    },
    {
        "matricula": "2021030136",
        "nombre": "Solis Velarde Oscar Alejandro",
        "grupo": "TI-73",
        "carrera": "Tecnologías de la Información",
        "foto": "/img/2021030136.jpg"
    },
    {
        "matricula": "2021030314",
        "nombre": "Peñaloza Pizarro Felipe Andrés",
        "grupo": "TI-73",
        "carrera": "Tecnologías de la Información",
        "foto": "/img/2021030314.jpg"
    },
    {
        "matricula": "2021030269",
        "nombre": "Salas Salazar Gael Mizraim",
        "grupo": "TI-73",
        "carrera": "Tecnologías de la Información",
        "foto": "/img/2021030269.jpg"
    },
    {
        "matricula": "osuna",
        "nombre": "Osuna Cardenas Melissa",
        "grupo": "TI-73",
        "carrera": "Tecnologías de la Información",
        "foto": "/img/osuna.jpg"
    },
    {
        "matricula": "2020030321",
        "nombre": "Ontiveros Govea Yair Alejandro",
        "grupo": "TI-73",
        "carrera": "Tecnologías de la Información",
        "foto": "/img/2020030321.jpg"
    },
    {
        "matricula": "2021030262",
        "nombre": "Qui Mora Ángel Ernesto",
        "grupo": "TI-73",
        "carrera": "Tecnologías de la Información",
        "foto": "/img/2021030262.jpg"
    },
    {
        "matricula": "2021030101",
        "nombre": "Plazola Arangure Yohan Alek",
        "grupo": "TI-73",
        "carrera": "Tecnologías de la Información",
        "foto": "/img/2021030101.jpg"
    }
];

// Elimina la lista de alumnos existente
ulElement.innerHTML = "";

// Recorre el arreglo de nuevos alumnos y crea un elemento li para cada uno
for (let i = 0; i < nuevosAlumnos.length; i++) {
    const alumnoActual = nuevosAlumnos[i];
    const liElement = document.createElement("li");
    liElement.className = "alumno";

    // Crea una estructura HTML con todos los campos del nuevo alumno
    liElement.innerHTML = `
        <img src="${alumnoActual.foto}" alt="${alumnoActual.nombre}" width="100">
        <div class="info">
            <h2>${alumnoActual.nombre}</h2>
            <ul>
                <li>Matrícula: ${alumnoActual.matricula}</li>
                <li>Grupo: ${alumnoActual.grupo}</li>
                <li>Carrera: ${alumnoActual.carrera}</li>
            </ul>
        </div>
    `;

    // Agrega el li al elemento ul
    ulElement.appendChild(liElement);
}