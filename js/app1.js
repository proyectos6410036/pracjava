document.getElementById("calcular").addEventListener("click", function () {
    const altura = parseFloat(document.getElementById("altura").value);
    const peso = parseFloat(document.getElementById("peso").value);

    if (isNaN(altura) || isNaN(peso) || altura <= 0 || peso <= 0) {
        alert("Ingresa valores válidos para altura y peso.");
        return;
    }

    const imc = peso / (altura * altura);

    let nivelIMC = "";
    let imagenURL = "";
    if (imc < 18.5) {
        nivelIMC = "Peso por debajo de lo normal";
        imagenURL = "/img/ima01.png"; // Reemplaza con la URL de la imagen correspondiente.
    } else if (imc >= 18.5 && imc < 25) {
        nivelIMC = "Peso saludable";
        imagenURL = "/img/ima02.png"; // Reemplaza con la URL de la imagen correspondiente.
    } else if (imc >= 25 && imc < 30) {
        nivelIMC = "Sobrepeso";
        imagenURL = "/img/ima03.png"; // Reemplaza con la URL de la imagen correspondiente.
    } else if (imc >= 30 && imc < 35) {
        nivelIMC = "Obesidad Tipo I";
        imagenURL = "/img/ima04.png"; // Reemplaza con la URL de la imagen correspondiente.
    } else if (imc >= 35 && imc < 40) {
        nivelIMC = "Obesidad Tipo II";
        imagenURL = "/img/ima05.png"; // Reemplaza con la URL de la imagen correspondiente.
    } else {
        nivelIMC = "Obesidad Tipo III";
        imagenURL = "/img/06.png"; // Reemplaza con la URL de la imagen correspondiente.
    }

    document.getElementById("resultado").innerHTML = `Tu IMC es: ${imc.toFixed(2)}<br>Nivel de IMC: ${nivelIMC}`;
    // Mostrar la imagen correspondiente.
    document.getElementById("imagenIMC").innerHTML = `<img src="${imagenURL}" alt="IMC Image">`;

    // Calcular las calorías diarias recomendadas.
    const edad = parseFloat(document.getElementById("edad").value);
const sexo = document.querySelector('input[name="sexo"]:checked').value;

let caloriasHombres = 0;
let caloriasMujeres = 0;
if (edad >= 10 && edad < 18) {
    caloriasHombres = 17.686 * peso + 658.2;
    caloriasMujeres = 13.384 * peso + 692.6;
} else if (edad >= 18 && edad < 30) {
    caloriasHombres = 15.057 * peso + 692.2;
    caloriasMujeres = 14.818 * peso + 486.6;
} else if (edad >= 30 && edad < 60) {
    caloriasHombres = 11.472 * peso + 873.1;
    caloriasMujeres = 8.126 * peso + 845.6;
} else if (edad >= 60) {
    caloriasHombres = 11.711 * peso + 587.7;
    caloriasMujeres = 9.082 * peso + 658.5;
}
document.getElementById("caloriasHombres").textContent = `Calorías diarias recomendadas para hombres: ${caloriasHombres.toFixed(2)}`;
document.getElementById("caloriasMujeres").textContent = `Calorías diarias recomendadas para mujeres: ${caloriasMujeres.toFixed(2)}`;
});
