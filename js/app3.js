
function llenarConNumerosAleatorios() {
  const cantidad = parseInt(document.getElementById("cantidad").value);
  const selectOpciones = document.getElementById("opciones");

  // Limpia las opciones del combo box y los resultados
  selectOpciones.innerHTML = "";
  
  let cantidadPares = 0;
  let cantidadImpares = 0;
  
  for (let i = 0; i < cantidad; i++) {
      const numero = Math.floor(Math.random() * 100) + 1; // Genera números aleatorios del 1 al 100

      // Agrega una opción al combo box con el número generado
      const opcion = document.createElement("option");
      opcion.value = numero;
      opcion.textContent = numero;
      selectOpciones.appendChild(opcion);

      if (numero % 2 === 0) {
          cantidadPares++;
      } else {
          cantidadImpares++;
      }
  }

  // Calcula el porcentaje de impares y pares
  const totalNumeros = cantidadPares + cantidadImpares;
  const porcentajeImpares = ((cantidadImpares / totalNumeros) * 100).toFixed(2);
  const porcentajePares = ((cantidadPares / totalNumeros) * 100).toFixed(2);

  // Actualiza el contenido de los elementos de resultados
  document.getElementById("porcentajeImpares").textContent = `Porcentaje de impares: ${porcentajeImpares}%`;
  document.getElementById("porcentajePares").textContent = `Porcentaje de pares: ${porcentajePares}%`;

  // Verifica si la lista es asimétrica y establece el contenido en función de eso
  const esAsimetrico = cantidadImpares !== cantidadPares;
  const esAsimetricoTexto = esAsimetrico ? "Es asimétrico" : "No es asimétrico";
  document.getElementById("esAsimetrico").textContent = esAsimetricoTexto;
}

